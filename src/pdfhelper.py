from pimento import menu
import os
import subprocess
import glob
import csv
import re
import itertools


def request_file_path(promt):
    """Requests the user for the path to a file, and checks if it exists."""
    while True:
        file_path = raw_input(promt).strip()
        if file_path.startswith('"') and file_path.endswith('"'):
            file_path = file_path[1:-1]
        if os.path.exists(file_path):
            return file_path


def parse_csv_file(csv_file_path):
    """Parse a document:page CSV file and filter out invalid symbols"""
    with open(csv_file_path, "rb") as csvFile:
        reader = csv.reader(csvFile, delimiter=";")
        rows = [("".join([c for c in document if re.match(r'[a-zA-Z0-9-_]', c)]), page) for document, page in reader]
    return rows


def split_pdf(input_file_path, output_file_path, split_range):
    """Calls pdfcat.py to split a PDF using a certain page-range"""
    subprocess.check_call(["python", "pdfcat.py", "-o", output_file_path, input_file_path, split_range])


def merge_pdfs(input_files, output_file_path):
    """Calls pdfcat.py to merge a set of PDF files"""
    try:
        subprocess.check_call(["python", "pdfcat.py", "-o", output_file_path] + input_files)
    except subprocess.CalledProcessError, e:
        print e.output
        exit(e.returncode)


def create_folder(folder_path):
    for x in itertools.count(1):
        try:
            os.mkdir(folder_path + ' (' + str(x) + ')')
            return folder_path + ' (' + str(x) + ')'
        except OSError:
            pass


def split(split_type):
    csv_file_path = request_file_path("Enter the path to the CSV file: ")
    pdf_file_path = request_file_path("Enter the path to the PDF file: ")

    documents = parse_csv_file(csv_file_path)
    output_path = os.path.join(os.path.dirname(pdf_file_path), "Split")
    output_path = create_folder(output_path)

    for index, document in enumerate(documents):
        if split_type == "Into Files":
            output_file_path = os.path.join(output_path, document[0] + ".pdf")
        if split_type == "Into Folders":
            output_file_path = os.path.join(output_path, document[0])
            os.mkdir(output_file_path)
            output_file_path = os.path.join(output_file_path, document[0] + ".pdf")
        try:
            split_range = str(int(document[1]) - 1) + ":" + str(int(documents[index + 1][1]) - 1)
        except IndexError:
            split_range = str(document[1]) + ":"
        progress = (index + 1) / (len(documents) / 100.0)
        print "[", "%02d%%" % progress, "]", "Splitting pages", split_range, "into", os.path.basename(output_file_path)
        split_pdf(pdf_file_path, output_file_path, split_range)


def merge():
    pdf_folder_path = request_file_path("Enter the path to the PDF folder: ")
    pdf_files = glob.glob(os.path.join(pdf_folder_path, "*.pdf"))
    pdf_files += glob.glob(os.path.join(pdf_folder_path, "*/*.pdf"))

    output_path = os.path.join(os.path.split(pdf_folder_path)[0], "Merge")
    output_path = create_folder(output_path)
    output_file_path = os.path.join(output_path, "Merge.pdf")

    print "Merging files into Merge.pdf.."
    merge_pdfs(pdf_files, output_file_path)


def main():
    option = menu(["Split", "Merge"], indexed=True, insensitive=True, fuzzy=True)
    if option == "Split":
        sub_option = menu(["Into Files", "Into Folders"], indexed=True, insensitive=True, fuzzy=True)
        split(sub_option)
    if option == "Merge":
        merge()
    raw_input("All done! Press enter to exit..")


if __name__ == '__main__':
    main()
